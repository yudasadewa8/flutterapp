// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, slash_for_doc_comments

import 'package:flutter/material.dart';
import 'package:myapp/login.dart';
import 'package:myapp/mygrid.dart';
import 'package:myapp/mygrid2.dart';
import 'package:myapp/qrscanner.dart';
import 'package:myapp/service/list_users_service.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'MAIN MENU'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),

      /**
       * bbody berisikan widget NormalLayout yang bisa dipanggil lebih singkat
       */
      body: Center(
        child: NormalLayout(),
      ),
    );
  }

  /**
   * extract widget agar dapat digunakan pada widget lain dan tidak terlalu banyak code yang sama (berulang)
   */
  Widget cardlist(
      String title, String subtitle, Color color, String nilai, Color bgColor) {
    return Card(
      color: bgColor,
      child: ListTile(
        title: Text(title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
        subtitle: Text(subtitle),
        trailing: Container(
          height: 50,
          width: 50,
          color: color,
          child: Center(
            child: Text(nilai,
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.white,
                    fontWeight: FontWeight.bold)),
          ),
        ),
      ),
    );
  }

  //extract widget layout
  Widget WideLayout1() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            height: 100.0,
            width: 100.0,
            color: Colors.red,
          ),
          Container(
            height: 100.0,
            width: 100.0,
            color: Colors.yellow,
          ),
        ],
      ),
    );
  }

/**
 * exctract widget untuk normalLayout (protrait)
 * berisikan tombol (menu) yang menuju halaman lain
 */
  Widget NormalLayout() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Center(
        child: GridView.count(
      primary: false,
      padding: const EdgeInsets.all(20),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: (width > 500) ? 3 : 2,
      children: <Widget>[
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => MyGrid()),
            );
          },
          child: Text(
            'Responsive Page',
            style: TextStyle(fontSize: 20),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => MyGrid2()),
            );
            // getUsers();
          },
          child: Text(
            'API Consume',
            style: TextStyle(fontSize: 20),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => Login()),
            );
            // getUsers();
          },
          child: Text(
            'LOGIN',
            style: TextStyle(fontSize: 20),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => Qrscanner()),
            );
            // getUsers();
          },
          child: Text(
            'QR-SCANNER',
            style: TextStyle(fontSize: 20),
          ),
        ),
      ],
    ));
  }
}
