// ignore_for_file: prefer_const_constructors, slash_for_doc_comments
//IMPORT PACKAGE
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:url_launcher/url_launcher.dart';
//-end IMPORT PACKAGE

class Qrscanner extends StatefulWidget {
  const Qrscanner({super.key});

  @override
  State<Qrscanner> createState() => _QrscannerState();
}

class _QrscannerState extends State<Qrscanner> {
  //membuat sebuah variabel bertipe String untuk menampung hasil scan qrcode
  String link = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("QrScanner"), //nama appBar
      ),

      /**
       * bagian body akan berisikan:
       * 1. hasil (Text) ==> menampung hasil scan qrcode
       * 2. tombol "Scan" (ElevatedButton) ==> membuka fitur scanner dari package flutter_barcode_scanner
       * 3. tombol "Buka Aplikasi" (ElevatedButton) ==> membuka url apabila hasil scan adalah url
       * 4. text "keterangan scan" (Text) ==> apabila hasil scan kosong/bukan url (berisi http:// atau https://) 
       */
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Text('hasil: $link'),
            SizedBox(
              height: 20,
            ),

            /**
             * tombol Scan ketika ditekan akan menampilkan screen scanner qrcode 
             * fungsi yang nantinya dijalankan menggunakan package flutter_barcode_scanner
             * FlutterBarcodeScanner.scanBarcode (
             *    String lineColor        ====> kode warna (ex. #ff6666)
             *    String cancelButtonText ====> teks untuk tombol back dari screen scanner 
             *    bool isShowFlashIcon    ====> true = menampilkan icon flash | false = tidak tampil icon flash
             *    ScanMode scanMode       ====> jenis scanmode (DEFAULT, QR, BARCODE)
             * )
             * setelah diperoleh hasil scan, maka String link yang dibuat sebelumnya akan diisi oleh hasil scan
             */
            ElevatedButton(
              onPressed: () async {
                String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
                    "#ff6666", "Tutup", false, ScanMode.DEFAULT);
                setState(() {
                  link = barcodeScanRes;
                });
              },
              child: Text(
                'Scan',
                style: TextStyle(fontSize: 50),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            /**
             * pengecekan apakah link kosong atau gagal melakukan scan apabila berisi string tersebut maka dicek lagi 
             * pengecekan kedua adalah pengecekan untuk variabel link apakah berisi string "https://" atau "http://"
             * apabila hasil scan adalah sebuah url, jika tombol ditekan maka memanggil fungsi _launchURL(string url)
             */
            (link != '' && link != '-1')
                ? (link.contains('https://') || link.contains('http://'))
                    ? ElevatedButton(
                        onPressed: () {
                          _launchURL(link);
                          print('ini link: ' + link);
                        },
                        child: Text(
                          'buka Url',
                          style: TextStyle(fontSize: 40),
                        ),
                      )
                    : Text('Hasil scan QR-code bukan merupakan url')
                : Text('Belum ada hasil scan')
          ],
        ),
      ),
    );
  }

  /**
   * fungsi ini membuka browser bawaan perangkat sesuai url yang diberikan
   */
  _launchURL(String url) async {
    if (!await launchUrl(
      Uri.parse(url),
      mode: LaunchMode.externalApplication,
    )) {
      throw "Tidak bisa membuka halaman";
    }
  }
}
