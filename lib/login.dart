// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, slash_for_doc_comments

import 'package:flutter/material.dart';
import 'package:myapp/service/list_users_service.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  /**
   * membuat variabel TextEditingController untuk menampung username dan password pada input form
   */
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),

      /**
       * body berisikan 2 input form untuk username dan password
       * untuk TextField berisikan parameter (
       *  - controller ---> variabel controller text 
       *  - decoration ---> style 
       *  - autofocus   --> field yang akan difokuskan diisi pertama (posisi pointer text)
       *  - obscureText --> true = text terlihat | false = text digantikan dengan simbol
       *  - obscuringCharacter --> simbol yang digunakan untuk mengganti obscureText
       * )
       * 
       * bagian tombol berisi "submit" dengan memanggil fungsi postLogin (username, password)
       */
      body: Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //Username
              Container(
                child: TextField(
                  controller: usernameController,
                  autofocus: true,
                  decoration: InputDecoration(
                      labelText: "Username",
                      labelStyle: TextStyle(
                        fontSize: 50,
                      )),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              //Password
              Container(
                child: TextField(
                  controller: passwordController,
                  obscureText: true,
                  obscuringCharacter: '*',
                  decoration: InputDecoration(
                      labelText: "Password",
                      labelStyle: TextStyle(
                        fontSize: 50,
                      )),
                ),
              ),

              SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () async {
                  postLogin(
                    usernameController.text,
                    passwordController.text,
                  );
                },
                child: Text('Login'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /**
   * fungsi postLogin diambil dari service ListUsersService dengan membuat variabel _service bertipe ListUsersService
   * agar dapat menggunakan semua fungsi-fungsi yang dimiliki oleh ListUserService()
   * kemudian dipanggil fungsi postLogin(username, password)
   */
  postLogin(String username, String password) async {
    ListUsersService _service = ListUsersService();
    await _service.postLogin(username, password);
  }
}
