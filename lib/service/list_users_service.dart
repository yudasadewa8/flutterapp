// ignore_for_file: slash_for_doc_comments

import "package:dio/dio.dart";
import 'package:myapp/model/list_users_model.dart';

/**
 * GET DATA
 * membuat class ListUserService untuk menampung seluruh fungsi-fungsi yang berkaitan dengan 
 * request get/post API dari url: reqres.in 
 */

class ListUsersService {
  Dio dio =
      Dio(); //membuat variabel bertipe Dio agar dapat menggunakan fungsi2 yang ada pada package dio

/**
 * Membuat fungsi untuk mengambil data user dari url:"https://reqres.in/api/users?page=2"
 */
  Future<List<ListUsersModel>?> getDataUsers() async {
    String url =
        "https://reqres.in/api/users?page=2"; //membuat variabel untuk menampung url
    final Response
        response; // membuat variabel response untuk menampung hasil request
    try {
      /**
       * nilai hasil get menggunakan package dio ditampung pada variabel dio dengan parameter url
       * menggunakan await agar dipastikan proses selesai dilakukan baru dilanjutkan ke code selanjutnya
       */
      response = await dio.get(
        url,
      );

      /**
       * variabel response memiliki beberapa properti seperti ==> response.statusCode, response.data, response.headers...
       * jika statusCode dari response bernilai 200 (success) maka data akan dieksekusi
       * json akan dicek apakah json merupakan Map dan json mengandung keys "data"
       * jika merupakan data Map,  maka dicek apakah json['data'] adalah sebuah list karena data dari server adalah list maka perlu dicek
       * jika data adalah list, maka data akan dimapping berdasarkan model ListUserModel
       */
      if (response.statusCode == 200) {
        var json = response.data;
        //boleh dipakai sesuai kondisi data json
        if (json is Map && json.keys.contains('data')) {
          var data = json['data'];
          if (data is List) {
            return data
                .map<ListUsersModel>((u) => ListUsersModel.fromJson(u))
                .toList();
          }
        }
      }
      return null;
    } on DioError catch (error, stacktrace) {
      print('Exception occured: $error stackTrace: $stacktrace');
      throw Exception(error.response);
    }
  }

/**
 * POST DATA (LOGIN)
 * fungsi postLogin untuk mengirim nilai username dan password yang nantinya akan dicek oleh server
 * 
 */
  postLogin(String username, String password) async {
    //membuat variabel untuk menampung url
    String url = "https://reqres.in/api/users";

    // membuat variabel response untuk menampung hasil request
    final Response response;

    /**
     * membuat body dari API dengan variabel formData bertipe FormData dengan mapping name dan job
     * name = username | job = password
     */
    FormData formData = FormData.fromMap({
      "name": username,
      "job": password,
    });

    /**
     * selanjutnya menggunakan try-catch untuk post data
     * hasil akan ditampung pada variabel response
     * menggunakan await agar memastikan proses selesai dikerjakan sebelum mengerjakan proses lain
     * menggunakan dio.post dengan parameter (String url, data : FormData)
     */
    try {
      response = await dio.post(
        url,
        data: formData,
      );
      print(response.data);
    } on DioError catch (error, stacktrace) {
      print('Exception occured: error stackTrace: stacktrace');
      throw Exception(error.response);
    }
  }
}
