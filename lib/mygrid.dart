// ignore_for_file: avoid_unnecessary_containers, slash_for_doc_comments

import "package:flutter/material.dart";

class MyGrid extends StatefulWidget {
  const MyGrid({super.key});

  @override
  State<MyGrid> createState() => _MyGridState();
}

class _MyGridState extends State<MyGrid> {
  /**
   * Menggunakan widget LayoutBuilder dengan constraint yang dapat digunakan sebagai parameter 
   * kondisi ukuran layar (landscape/potrait) atau berdasarkan ukuran tertentu
   * constraint bisa menggunakan properti maxWidht = ukuran maksimum lebar screen perangakat
   * jika pada nilai constraint.maxWidth > 500 maka akan menampilkan widget tampilanLebar()
   * sedangkan nilainya kurang dari 500 akan menampilkan widget tampilanNormal() 
   */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: LayoutBuilder(
        builder: (context, constraint) {
          if (constraint.maxWidth > 500) {
            return tampilanLebar();
          } else {
            return tampilanNormal();
          }
        },
      ),
    );
  }

  Widget tampilanLebar() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: 100,
            width: 100,
            color: Colors.amber,
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.purple,
          ),
        ],
      ),
    );
  }

  Widget tampilanNormal() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: 100,
            width: 100,
            color: Colors.teal,
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.red,
          ),
        ],
      ),
    );
  }
}
