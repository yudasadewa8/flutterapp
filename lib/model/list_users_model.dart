// ignore_for_file: slash_for_doc_comments

/**
 * membuat class ListUserModel yang berperan sebagai model data;
 * di class ListUserModel berisikan constructor sesuai variabel yang digunakan;
 * terdapat fungsi factory dan map untuk mengolah data json yang diperoleh dari hasil request
 * kemudian hasilnya akan dimapping
 */
class ListUsersModel {
  ListUsersModel({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.avatar,
  });

  int? id;
  String? email;
  String? firstName;
  String? lastName;
  String? avatar;

  factory ListUsersModel.fromJson(Map<String, dynamic> json) => ListUsersModel(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        avatar: json["avatar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "first_name": firstName,
        "last_name": lastName,
        "avatar": avatar,
      };
}
