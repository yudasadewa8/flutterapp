// ignore_for_file: avoid_unnecessary_containers, slash_for_doc_comments, prefer_const_constructors

//IMPORT PACKAGE
import "package:flutter/material.dart";
import 'package:myapp/model/list_users_model.dart';
import 'package:myapp/service/list_users_service.dart';
//end IMPORT PACKAGE

class MyGrid2 extends StatefulWidget {
  const MyGrid2({super.key});

  @override
  State<MyGrid2> createState() => _MyGrid2State();
}

class _MyGrid2State extends State<MyGrid2> {
  /**
   *  1. buat variabel list user model
  */
  List<ListUsersModel> _listUser = [];

  /**
   * 2. buat fungsi get data user dengan menggunakan service yang ada pada /lib/service/list_user_service.dart pada fungsi getDatausers()
   *    - membuat sebuah variabel bertipe class ListUsersService agar dapat menggunakan semua fungsi yang ada pada 
   *      class ListUserService dengan hanya menggunakan variabel
   *    - dengan variabel _service dipanggil fungsi getDataUsers() 
   *    - jika diperoleh data (value) maka akan diset nilai _listUser  dengan value
   */
  getUsers() async {
    ListUsersService _service = ListUsersService();
    await _service.getDataUsers().then((value) {
      setState(() {
        _listUser = value!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    //membuat variabel bertipe double untuk mendapatkan lebar dari screen perangkat
    double lebar = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(),

      /**
       * pada bagian body akan terdapat:
       *  1. tombol untuk memanggil fungsi getUsers() dan mengambil data user dari https:reqres.in
       *  2. list user  menggunakan data yang sudah diperoleh dari hasil getUsers()
       *  3. menggunakan ListView.builder dengan parameter (
       *      - itemCount => panjang list
       *      - itemBuilder => widget
       *      )
       */
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                getUsers();
              },
              child: Text(
                'get user',
                style: TextStyle(fontSize: 50),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: _listUser.length,
                  itemBuilder: (context, index) {
                    ListUsersModel data = _listUser[index];
                    return cardlist(data.id!.toString(), data.firstName!,
                        Colors.red, data.avatar!, Colors.grey.shade100);
                  }),
            ),
          ],
        ),
      ),
    );
  }

  /**
   * extract widget agar dapat digunakan pada widget lain dan tidak terlalu banyak code yang sama (berulang)
   */
  Widget cardlist(String title, String subtitle, Color color, String avatar,
      Color bgColor) {
    return Card(
      color: bgColor,
      child: ListTile(
        title: Text(title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
        subtitle: Text(subtitle),
        trailing: Container(
          height: 50,
          width: 50,
          color: color,
          child: Center(
            child: Image.network(
              avatar,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
